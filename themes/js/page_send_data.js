/*********************************
**                              **
**           JH                 **
**                              **
**                              **
*********************************/

//跳转页面
let sendURLData =function (url,parameter) {
	var str=JSON.stringify(parameter);
	if (str.length<1600) {
		window.location =url+"?"+str;
	}else{
		console.log("传递数据过长,跳转失败");
	}
}


//接收数据
let acceptURLData =function (){
	try{
	let str =window .location.search;
	str= str.substr(1,str.length-1);
	 str=str.replace(/\%22/g,"\"");
	 str = str.replace(/\%20/g," ");
	  var returnData =JSON.parse(str);
	  return {"code":"1","data":returnData};
	} catch{
	  //alert("反序列化失败，请检查传递的数据是否正常.")
	  return {"code":"0","data":"反序列化失败，请检查传递的数据是否正常."};
	}
}
 // <!-- localStorage方式存储（无时间限制）-->
//储存数据
let localStorageSetData =function (key1,value1) {
	if(typeof(Storage)!=="undefined"){
		localStorage.setItem(key1,value1);
		return {"code":"1","data":"储存成功"};
    } else {
        return {"code":"0","data":"抱歉,你当前的浏览器不支持Storage"};
    }
}
//查询数据
let localStorageGetData=function (key1) {
	if(typeof(Storage)!=="undefined"){
		return {"code":"1","data":localStorage.getItem(key1)};
    } else {
        return {"code":"0","data":"抱歉,你当前的浏览器不支持Storage"};
    }
}

//删除数据
let localStorageRemoveData=function (key1) {
	if(typeof(Storage)!=="undefined"){
		localStorage.removeItem(key1);
		return {"code":"1","data":"删除成功"};
    } else {
        return {"code":"0","data":"抱歉,你当前的浏览器不支持Storage"};
    }
}

//删除所有数据
let localStorageClearData=function () {
	if(typeof(Storage)!=="undefined"){
		localStorage.clear();
		return {"code":"1","data":"删除成功"};
	} else {
		return {"code":"0","data":"抱歉,你当前的浏览器不支持Storage"};
    }
}

//获取下标元素的键名
let localStorageGetKey=function (index1) {
	if(typeof(Storage)!=="undefined"){
		return {"code":"1","data":localStorage.key(index1)};
	} else {
		return {"code":"0","data":"抱歉,你当前的浏览器不支持Storage"};
    }
}



 // <!-- sessionStorage方式存
 //(关闭浏览器或者跳转到其他域名页面或者其他IP页面数据自动清除）-->

 let sessionStorageSetData =function (key1,value1) {
	if(typeof(Storage)!=="undefined"){
		sessionStorage.setItem(key1,value1);
		return {"code":"1","data":"储存成功"};
    } else {
        return {"code":"0","data":"抱歉,你当前的浏览器不支持Storage"};
    }
}
//查询数据
let sessionStorageGetData=function (key1) {
	if(typeof(Storage)!=="undefined"){
		return {"code":"1","data":sessionStorage.getItem(key1)};
    } else {
        return {"code":"0","data":"抱歉,你当前的浏览器不支持Storage"};
    }
}

//删除数据
let sessionStorageRemoveData=function (key1) {
	if(typeof(Storage)!=="undefined"){
		sessionStorage.removeItem(key1);
		return {"code":"1","data":"删除成功"};
    } else {
        return {"code":"0","data":"抱歉,你当前的浏览器不支持Storage"};
    }
}

//删除所有数据
let sessionStorageClearData=function () {
	if(typeof(Storage)!=="undefined"){
		sessionStorage.clear();
		return {"code":"1","data":"删除成功"};
	} else {
		return {"code":"0","data":"抱歉,你当前的浏览器不支持Storage"};
    }
}

//获取下标元素的键名
let sessionStorageGetKey=function (index1) {
	if(typeof(Storage)!=="undefined"){
		return {"code":"1","data":sessionStorage.key(index1)};
	} else {
		return {"code":"0","data":"抱歉,你当前的浏览器不支持Storage"};
    }
}
